<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="format-detection" content="telephone=no">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!--[if lt IE 9]><script type="text/javascript" src="/js/html5.js"></script><![endif]-->
    <!--[if lt IE 9]><script type="text/javascript" src="/js/placeholder.js"></script><![endif]-->
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '530795200595249');
        fbq('track', 'PageView');
    </script>
    <link rel="shortcut icon" href="/favicon.ico">
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=530795200595249&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
<?php $this->beginBody() ?>
<?php
$lngs=\backend\models\Words::find()->where(['page_id'=>0])->all();
$str=[];
foreach ($lngs as $lng){
    $str[$lng['id']]=$lng['text'];
}

$settings=\backend\models\Settings::find()->all();
$set=[];
foreach ($settings as $setting) {
    $set[$setting['id']]=$setting['value'];
}
?>
<div class="fixed-top">
    <div class="holder">
        <div class="fixed-logo">
            <a href="/"><img src="/images/fixed-logo.png" alt=""></a>
        </div>
        <div class="fixed-nav">
            <ul class="fixed-nav-list">
                <?php
                $menu=[
                    '/'=>'Award',
                    '/symbol'=>'The symbol',
                    '/benefits'=>'Benefits',
                    '/how-it-work'=>'How it works',
                    '/winners'=>'Winners',
                    '/news'=>'News',
                ];
                foreach ($menu as $key=>$value){
                ?>
                <li><a href="<?=$key?>"><?=$value?></a></li>
                <?php } ?>
                <li class="nav-gold"><a href="#join_us" class="fancybox">apply now</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="fixed-bot fixed">
    <div class="holder">
        <div class="fixed-webs">
            <a href="<?=$set[2]?>" target="_blank" class="fixed-web web1"></a>
            <a href="<?=$set[3]?>" target="_blank" class="fixed-web web2"></a>
            <a href="<?=$set[4]?>" target="_blank" class="fixed-web web6"></a>
        </div>
        <div class="fixed-link fixed-link2">
            <a href="<?=$set[5]?>"><?=$str[45]?></a>
        </div>
        <div class="fixed-link fixed-link1">
            <a href="tel:+16468108764"><?=$str[38]?></a>
        </div>
    </div>
</div>
<a href="" class="fixed-scroll-top"><img src="/images/arrow-top.png" alt=""></a>
<div id="wrapper">
    <header id="header">
        <div class="header-mob">
            <a href="" class="nav-mob"><img src="/images/nav-icon.png" alt=""> Menu</a>
            <a href="/" class="logo-mob"><img src="/images/logo-mob.png" alt=""></a>
            <a href="#join_us" class="button fancybox">Apply now</a>

            <div class="mob-nav mob-nav1">
                <img src="/images/logo-mob.png" class="logos" alt="Logo">
                <a href="" class="close-mob"><img src="/images/nav-close.png" alt=""></a>
                <ul class="mob-nav-top">
                    <?php
                    foreach ($menu as $key=>$value){
                        ?>
                        <li><a href="<?=$key?>"><?=$value?></a></li>
                    <?php } ?>
                    <li class="gold"><a href="#join_us" class="fancybox">Apply now</a></li>
                </ul>

                <div class="text-center">
                    <a href="<?=$set[2]?>" target="_blank" class="footer-web web1"></a>
                    <a href="<?=$set[3]?>" target="_blank" class="footer-web web2"></a>
                    <a href="<?=$set[4]?>" target="_blank" class="footer-web web6"></a>
                </div>
                <div class="footer-contact-phone mob-nav-phone"><a href="tel:<?=$str[38]?>"><i></i> <?=$str[38]?></a></div>
<!--                <div class="footer-signup-form">-->
<!--                    --><?php
//                    $sub=new \backend\models\Subscribe();
//                    $form=\yii\bootstrap\ActiveForm::begin(['action'=>'/site/subscribe']);
//                    ?>
<!--                    <fieldset>-->
<!--                        <div class="signup-input-holder">-->
<!--                            --><?php
//                            echo $form->field($sub, 'email')->textInput(['class'=>'signup-input', 'placeholder'=>$str[35]])->label(false);
//                            ?>
<!--                        </div>-->
<!--                        <button class="signup-button">--><?//=$str[36]?><!--</button>-->
<!--                    </fieldset>-->
<!--                    --><?// \yii\bootstrap\ActiveForm::end(); ?>
<!--                </div>-->
            </div>
        </div>
        <div class="header-nav">
            <div class="holder">
                <a href="/" class="logo"><img src="/images/logo.png" alt=""></a>
                <div class="header-nav-holder">
                    <ul>
                        <?php
                        foreach ($menu as $key=>$value){
                            ?>
                            <li><a href="<?=$key?>"><?=$value?></a></li>
                        <?php } ?>
                        <li class="nav-gold"><a href="#join_us" class="fancybox">apply now</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <?=$content?>
</div>

<footer id="footer">
<!--    <div class="text-center help-main">Have questions or need help?</div>-->
<!--    <div class="text-center" style="margin-bottom: 30px;"><button id="show-help" class="button">Contact us</button></div>-->
    <div class="holder">
        <div class="footer-block">
            <div class="footer-signup">
                <h4><?=$str[33]?></h4>
                <div class="footer-signup-text"><?=$str[34]?></div>
                <div class="footer-signup-form">
                    <?php
                    $sub=new \backend\models\Subscribe();
                    $form=\yii\bootstrap\ActiveForm::begin(['action'=>'/site/subscribe']);
                    ?>
                        <fieldset>
                            <div class="signup-input-holder">
                                <?php
                                echo $form->field($sub, 'email')->textInput(['class'=>'signup-input', 'placeholder'=>$str[35]])->label(false);
                                ?>
                            </div>
                            <button class="signup-button"><?=$str[36]?></button>
                        </fieldset>
                    <? \yii\bootstrap\ActiveForm::end(); ?>
                </div>
            </div>
            <div class="footer-contact">
                <h4><?=$str[37]?></h4>
                <div class="footer-contact-phone"><a href="tel:<?=$str[38]?>"><?=$str[38]?></a></div>
                <div class="footer-contact-text"><b><?=$str[39]?></b><br>
                    <?=$str[40]?></div>
            </div>
            <div class="footer-webs">
                <h4><?=$str[41]?></h4>
                <div class="footer-webs-holder">
                    <a href="<?=$set[2]?>" target="_blank" class="footer-web web1"></a>
                    <a href="<?=$set[3]?>" target="_blank" class="footer-web web2"></a>
                    <a href="<?=$set[4]?>" target="_blank" class="footer-web web6"></a>
                </div>
            </div>
        </div>
        <div class="footer-nav">
            <ul>
                <?php
                foreach ($menu as $key=>$value){
                    ?>
                    <li><a href="<?=$key?>"><?=$value?></a></li>
                <?php } ?>
                <li class="gold"><a href="#join_us" class="fancybox">apply now</a></li>
            </ul>
        </div>
        <div class="footer-copy">
            <div class="copy">&copy; <?=$str[42]?>
                <script type="text/javascript">var mdate = new Date(); document.write(mdate.getFullYear());</script>
                , <a href="<?=$set[6]?>">Terms&amp;Conditions</a>,
                <a href="<?=$set[7]?>" class="fancybox">Privacy Policy</a><br>
                <?=$str[43]?></div>
            <div class="footer-copy-bottom">
                <img src="/images/flag.png" alt="" class="footer-copy-flag">
                <?=$str[44]?>
            </div>
        </div>
    </div>
</footer>
<div class="backdoor"></div>
<div class="subscribe-modal">
    <img src="/images/close-icon.png" class="close">
    <img src="/images/pero.png" alt="Logo">
    <div class="title">Become a Luxury Lifestyle Awards Member
        for FREE and Build you Luxury brand</div>
    <div class="text">
        Get access to exclusive articles, marketing tools, invitations
        to networking events and promote your business in
        membership community
    </div>
    <form id="subscribe">
        <input type="text" name="name" required class="user validate[required,custom[onlyLetterSp],length[3,100]]" placeholder="your first name">
        <input type="text" name="email" required class="email validate[required,custom[email]]" placeholder="your email address">
        <button class="btn-modal">Join now</button>
    </form>
</div>

<div class="modal-thank">
    <img src="/images/logo-modal.png" alt="Logo">
    <div class="title">Thank you!</div>
    <div class="text">We have received your application form. Our manager will follow up
        with you shortly to help understand awarding process and help you fill out
        and extended application form participation in LLA.</div>
    <div class="intro">We believe in you!
        You could be our next winner!</div>
</div>

<div class="popup" id="join_us">
    <div class="popup-title"><?=$str[29]?></div>
    <div class="popup-text"><?=$str[30]?></div>
    <?php
    $model=new \backend\models\Orders();
    $form=\yii\bootstrap\ActiveForm::begin(['action'=>'/site/order-save']);
    ?>
        <fieldset>
            <div class="form-col">
                <?=$form->field($model, 'name')->textInput(['class'=>'input', 'placeholder'=>'Full name *'])->label(false); ?>
            </div>
            <div class="form-col form-col-right">
                <?=$form->field($model, 'company')->textInput(['class'=>'input', 'placeholder'=>'Company *'])->label(false); ?>
            </div>
            <div class="form-col">
                <?=$form->field($model, 'email')->textInput(['class'=>'input', 'placeholder'=>'Corporate email address *'])->label(false); ?>
            </div>
            <div class="form-col form-col-right">
                <?=$form->field($model, 'phone')->textInput(['class'=>'input', 'placeholder'=>'Phone number *', 'type'=>'number'])->label(false); ?>
            </div>
            <?=$form->field($model, 'web')->textInput(['class'=>'input', 'placeholder'=>'Website address *'])->label(false); ?>
            <?=$form->field($model, 'comment')->textarea(['class'=>'textarea', 'placeholder'=>'Comments', 'rows'=>10, 'cols'=>30])->label(false); ?>
            <div class="popup-check">
                <div class="check-block">
                    <div class="checker" id="uniform-check1"><span><input type="checkbox" name="Orders[subscribe]" class="check" id="check1" checked></span></div>
                    <label for="check1" class="checkbox-label"><?=$str[31]?></label>
                </div>
            </div>
            <div class="form-button">
                <button class="submit"><?=$str[32]?></button>
            </div>
        </fieldset>
    <? \yii\bootstrap\ActiveForm::end(); ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
