<?php
$seo=\backend\models\Seo::find()->where(['element'=>'pages', 'element_id'=>7])->one();
$this->title = $seo->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $seo->description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $seo->keywords
]);

$page=\backend\models\Pages::findOne(7);
$word=\backend\models\Words::find()->where(['page_id'=>7])->all();

$page_image=json_decode($page['image']);
$pics=json_decode($page['bg']);
?>
<section class="main">
    <div class="bg-slider bg-slider-mini">
        <?php
        if (count($pics)<=0) {
            ?>
            <div class="item"><img src="/source/pages/banner1.jpg" alt=""></div>
            <?php
        }
        foreach ($pics as $pic){
            ?>
            <div class="item"><img src="<?= $pic ?>" alt=""></div>
            <?php
        }
        ?>
    </div>
    <div class="holder">
        <div class="title">
            <h1><?=$page['title']?></h1>
        </div>
        <div class="symbol-top-info"><?=$word[0]['text']?></div>
        <div class="table-block">
            <table>
                <tr>
                    <th>&nbsp;</th>
                    <th>Silver</th>
                    <th>Gold </th>
                    <th>Platinum</th>
                    <th class="gap"></th>
                </tr>
                <tr>
                    <td>Winners are eligible to use Winner's official logo in their advertising campaigns</td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td class="gap"></td>
                </tr>
                <tr>
                    <td>The official trophy - the Golden Crown</td>
                    <td><span class="table-dot"></span></td>
                    <td><span class="table-dot"></span></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td class="gap"></td>
                </tr>
                <tr>
                    <td>Provision of the Winners certificate</td>
                    <td><span class="table-dot"></span></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td><span class="table-dot"></span></td>
                    <td class="gap"></td>
                </tr>
                <tr>
                    <td>Provision of the Platinum certificate</td>
                    <td><span class="table-dot"></span></td>
                    <td><span class="table-dot"></span></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td class="gap"></td>
                </tr>
                <tr>
                    <td>Provision of the Winners digital certificate</td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td class="gap"></td>
                </tr>
                <tr>
                    <td>Package of merchandising materials for promotion (Stickers, Flag, Pendant)</td>
                    <td><span class="table-dot"></span></td>
                    <td><span class="table-dot"></span></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td class="gap"></td>
                </tr>
                <tr>
                    <td>Personalized press-release on victory</td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td class="gap"></td>
                </tr>
                <tr>
                    <td>Personalized announcement on the Аwards official website and Social Media channels</td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td class="gap"></td>
                </tr>
                <tr>
                    <td>Interview with the Winner published on the Awards official website and Social Media channels</td>
                    <td><span class="table-dot"></span></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td class="gap"></td>
                </tr>
                <tr>
                    <td>Opportunity to place press-releases of the company on the Awards official website and Social Media channels</td>
                    <td><span class="table-dot"></span></td>
                    <td>5</td>
                    <td>Unlimited</td>
                    <td class="gap"></td>
                </tr>
                <tr>
                    <td>Incorporation of Winner's logo in your promotional materials (Brochures, Catalogues, Website)</td>
                    <td><span class="table-dot"></span></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td><img src="images/table-arrow.png" alt=""></td>
                    <td class="gap"></td>
                </tr>
                <tr class="table-bottom">
                    <td>Cost</td>
                    <td>$650 </td>
                    <td>$950</td>
                    <td colspan="2">$1650</td>
                </tr>
            </table>
        </div>
    </div>
</section>