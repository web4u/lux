<?php
$seo=\backend\models\Seo::find()->where(['element'=>'pages', 'element_id'=>3])->one();
$this->title = $seo->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $seo->description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $seo->keywords
]);

$page=\backend\models\Pages::findOne(3);


$word=\backend\models\Words::find()->where(['page_id'=>3])->all();
?>
<section class="main">
    <div class="bg-slider bg-slider-mini">
        <?php
        $page_image=json_decode($page['image']);
        $pics=json_decode($page['bg']);
        foreach ($pics as $pic){
            ?>
            <div class="item"><img src="<?= $pic ?>" alt=""></div>
            <?php
        }
        ?>
    </div>
    <div class="holder">
        <div class="title">
            <h1><?=$page['title']?></h1>
        </div>
        <div class="benefits">
            <?php
            $blocks=\backend\models\Blokitems::find()->where(['blok_id'=>1])->all();
            foreach ($blocks as $block){
            ?>
            <div class="benefits-item">
                <div class="benefits-icon benefits-icon1"><img src="<?=json_decode($block['image'])[0]?>" alt=""></div>
                <div class="benefits-text"><?=strip_tags($block['text'])?></div>
            </div>
            <?php } ?>
        </div>
        <div class="package">
            <h2><?=strip_tags($word[0]['text'])?></h2>
            <div class="subtitle"><?=strip_tags($word[1]['text'])?></div>
            <div class="subtitle package-bottom-text"><?=strip_tags($word[2]['text'])?></div>
            <div class="package-block">
                <?php
                $color=['package1','package2', 'package3 recommend'];
                $k=0;
                foreach ($packages as $package) {
                ?>
                <div class="package-item <?=$color[$k]?>">
                    <div class="package-name"><?=$package['name']?></div>
                    <div class="package-for"><?=$package['intro']?></div>
                    <div class="package-price"><?=$package['price']?></div>
                    <div class="package-button">
                        <a href="/benefits/<?=$package['id']?>" class="button fancybox">learn more</a>
                    </div>
                </div>
                <? $k++; } ?>
            </div>
            <div class="package-bottom-link">
                <a href="/compare-packages">Compare packages</a>
            </div>
        </div>
        <div class="button-holder"><a href="#join_us" class="button fancybox">Apply now</a></div>
    </div>
</section>