<?php

/* @var $this yii\web\View */
$seo=\backend\models\Seo::find()->where(['element'=>'pages', 'id'=>1])->one();
$this->title = $seo->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $seo->description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $seo->keywords
]);

$word=\backend\models\Words::find()->where(['page_id'=>1])->all();
?>
<section class="main">
    <div class="item item-ony-mobile"><img src="/images/banner1-m.jpg" alt=""></div>
    <div class="bg-slider bg-slider-index">
        <?php
        $page=\backend\models\Pages::findOne(1);
        $page_image=json_decode($page['image']);
        $pics=json_decode($page['bg']);
        foreach ($pics as $pic){
            ?>
            <div class="item"><img src="<?= $pic ?>" alt=""></div>
            <?php
        }
        ?>
    </div>
    
    <div class="holder">
        <div class="title">
            <h1><?=$page['title']?></h1>
        </div>
        <div class="awards-why-text"><?=$word[0]['text']?></div>
    </div>
    <div class="benefits-big">
        <div class="holder">
            <div class="title">
                <h2><?=$word[1]['text']?></h2>
            </div>
            <div class="benefits">
                <?php
                $itemsb=\backend\models\Blokitems::find()->where(['blok_id'=>5])->all();
                foreach ($itemsb as $imb){
                ?>
                <div class="benefits-item">
                    <div class="benefits-icon benefits-icon1"><img src="<?=json_decode($imb['image'])[0]?>" alt=""></div>
                    <div class="benefits-text"><strong><?=$imb['text']?></strong>
                        <?=$imb['other_text']?></div>
                </div>
                <? } ?>
            </div>
        </div>
    </div>
    <div class="holder">
        <div class="our-winners">
            <h2><?=$word[2]['text']?></h2>
            <div class="subtitle"><?=$word[3]['text']?></div>
            <div class="our-winners-text"><?=$word[4]['text']?></div>
            <div class="brands-block">
                <div class="brands-item"><img src="/source/winners/1.png" alt="Dom"></div><div class="brands-item"><img src="/source/winners/2.png" alt="Quintessentially"></div><div class="brands-item"><img src="/source/winners/3.png" alt="Chanel"></div><div class="brands-item"><img src="/source/winners/4.png" alt="Tiffany"></div><div class="brands-item"><img src="/source/winners/5.png" alt="AP"></div><div class="brands-item"><img src="/source/winners/6.png" alt="Ferrari"></div><div class="brands-item"><img src="/source/winners/7.png" alt="Stay"></div><div class="brands-item"><img src="/source/winners/8.png" alt="Zuma"></div><div class="brands-item"><img src="/source/winners/411.jpg" alt="Tiff"></div><div class="brands-item"><img src="/source/winners/9.png" alt="Messien"></div><div class="brands-item"><img src="/source/winners/10.png" alt="Thomas"></div><div class="brands-item"><img src="/source/winners/11.png" alt="Ritz"></div><div class="brands-item"><img src="/source/winners/12.png" alt="Hilton"></div><div class="brands-item"><img src="/source/winners/13.png" alt="Inter"></div><div class="brands-item"><img src="/source/winners/14.png" alt="Roll Roys"></div>            </div>
        </div>
        <div class="map-block">
            <h2><?=$word[5]['text']?></h2>
            <div class="map-scheme">
                <img src="<?=$page_image[0]?>" alt="">
            </div>
            <div class="map-column">
                <div class="map-item"><b><span class="count-to" data-from="0" data-to="<?=strip_tags($word[6]['text'])?>" data-speed="1500"><?=strip_tags($word[6]['text'])?></span>+</b>
                    <?=strip_tags($word[7]['text'])?></div>
                <div class="map-item"><b><?=strip_tags($word[8]['text'])?>+ </b>
                    <?=strip_tags($word[9]['text'])?></div>
                <div class="map-item"><b><span class="count-to" data-from="0" data-to="<?=strip_tags($word[10]['text'])?>" data-speed="1000"><?=strip_tags($word[10]['text'])?></span>+ </b>
                    <?=strip_tags($word[11]['text'])?></div>
                <div class="map-item"><b><?=strip_tags($word[12]['text'])?> </b>
                    <?=strip_tags($word[13]['text'])?></div>
            </div>
        </div>
        <div class="academy">
            <h2><?=$word[14]['text']?></h2>
            <div class="academy-logo"><img src="<?=$page_image[1]?>" alt=""></div>
            <div class="academy-text">
                <?=$word[15]['text']?>
            </div>
            <div class="human-slider">
                <?=$emp?>
            </div>
        </div>
        <div class="quote-block">
            <h2><?=$word[16]['text']?></h2>
            <div class="quote-slider">
                <?=$term?>
            </div>
        </div>
        <div class="media">
            <h2><?=$word[17]['text']?></h2>
            <div class="brands-block">
                <?=$media?>
            </div>
        </div>
        <div class="button-holder"><a href="#join_us" class="button fancybox">Apply now</a></div>
    </div>
</section>
