<?php
$seo=\backend\models\Seo::find()->where(['element'=>'packages', 'element_id'=>$id])->one();
$this->title = $seo->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $seo->description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $seo->keywords
]);
?>
<section class="main">
    <div class="bg-slider bg-slider-mini">
        <?php
        $page=\backend\models\Pages::findOne(3);
        $page_image=json_decode($page['image']);
        $pics=json_decode($page['bg']);
        foreach ($pics as $pic){
            ?>
            <div class="item"><img src="<?= $pic ?>" alt=""></div>
            <?php
        }
        ?>
    </div>
    <div class="holder">
        <div class="title">
            <h1><?=$pack['name']?> package</h1>
        </div>
        <div class="package-info">
            <?php
            $k=1;
            foreach ($pack_items as $pack_item) {
                $class=$k%2==0?'':'package-reverse';
            ?>
            <div class="package-info-block <?=$class?>">
                <div class="package-info-item">
                    <img src="<?=json_decode($pack_item['image'])[0]?>" alt="">
                </div>
                <div class="package-info-item">
                    <div class="package-info-inner"><?=$pack_item['text']?></div>
                </div>
            </div>
            <? $k++; } ?>
        </div>
        <div class="package-line">
            <div class="package-line-holder">
                <div class="package-line-name">
                    <span><?=$pack['name']?></span>
                    <?=$pack['intro']?>
                </div>
                <div class="package-line-price"><?=$pack['price']?></div>
                <?php
                if ($pack['recomended']==1){
                ?>
                    <div class="package-line-text">reccomended</div>
                <? } ?>
            </div>
            <div class="package-line-bottom"><?=$pack['desc']?></div>
        </div>

        <div class="package-block package-grey">
            <?php
            $color=['package1','package2', 'package3 recommend'];
            $k=0;
            foreach ($packages as $package) {
                ?>
                <div class="package-item <?=$color[$k]?>">
                    <div class="package-name"><?=$package['name']?></div>
                    <div class="package-for"><?=$package['intro']?></div>
                    <div class="package-price"><?=$package['price']?></div>
                    <div class="package-button">
                        <a href="/benefits/<?=$package['id']?>" class="button">learn more</a>
                    </div>
                </div>
                <? $k++; } ?>
        </div>
    </div>
</section>