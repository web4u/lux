<?php
$seo=\backend\models\Seo::find()->where(['element'=>'pages', 'id'=>1])->one();
$this->title = $seo->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $seo->description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $seo->keywords
]);

$page=\backend\models\Pages::findOne(2);
$pic=json_decode($page['image']);
$word=\backend\models\Words::find()->where(['page_id'=>2])->all();
?>
<section class="main">
    <div class="bg-slider bg-slider-mini">
        <?php
        $page=\backend\models\Pages::findOne(2);
        $page_image=json_decode($page['image']);
        $pics=json_decode($page['bg']);
        foreach ($pics as $pi){
            ?>
            <div class="item"><img src="<?= $pi ?>" alt=""></div>
            <?php
        }
        ?>
    </div>
    <div class="holder">
        <div class="title">
            <h1><?=$page['title']?></h1>
        </div>
        <div class="symbol">
            <div class="symbol-top">
                <div class="symbol-top-logo"><img src="<?=$pic[0]?>" alt=""></div>
                <div class="symbol-top-text"><?=$word[0]['text']?></div>
            </div>
            <div class="symbol-top-info"><?=$word[1]['text']?></div>
            <div class="symbol-img">
                <div class="symbol-img-min"><img src="<?=$pic[1]?>" alt=""></div>
                <div class="symbol-img-big"><img src="<?=$pic[2]?>" alt=""></div>
            </div>
            <div class="symbol-info">
                <?=$word[2]['text']?>
            </div>
        </div>
        <?php
        $ben=\backend\models\Bloks::findOne(1);
        ?>
        <div class="title">
            <h2><?=$ben['name']?></h2>
        </div>
        <div class="benefits">
            <?php
            $bitem=\backend\models\Blokitems::find()->where(['blok_id'=>1])->all();
            foreach ($bitem as $bit){
                $img=json_decode($bit['image']);
            ?>
            <div class="benefits-item">
                <div class="benefits-icon benefits-icon1"><img src="<?=$img[0]?>" alt=""></div>
                <div class="benefits-text"><?=$bit['text']?></div>
            </div>
            <? } ?>
        </div>
        <div class="brands">
            <?php
            $blok=\backend\models\Bloks::findOne(2);
            ?>
            <h2><?=$blok['name']?></h2>
            <div class="brands-block">
                <div class="brands-item"><img src="/source/winners/1.png" alt="Dom"></div><div class="brands-item"><img src="/source/winners/2.png" alt="Quintessentially"></div><div class="brands-item"><img src="/source/winners/3.png" alt="Chanel"></div><div class="brands-item"><img src="/source/winners/4.png" alt="Tiffany"></div><div class="brands-item"><img src="/source/winners/5.png" alt="AP"></div><div class="brands-item"><img src="/source/winners/6.png" alt="Ferrari"></div><div class="brands-item"><img src="/source/winners/7.png" alt="Stay"></div><div class="brands-item"><img src="/source/winners/8.png" alt="Zuma"></div><div class="brands-item"><img src="/source/winners/411.jpg" alt="Tiff"></div><div class="brands-item"><img src="/source/winners/9.png" alt="Messien"></div><div class="brands-item"><img src="/source/winners/10.png" alt="Thomas"></div><div class="brands-item"><img src="/source/winners/11.png" alt="Ritz"></div><div class="brands-item"><img src="/source/winners/12.png" alt="Hilton"></div><div class="brands-item"><img src="/source/winners/13.png" alt="Inter"></div><div class="brands-item"><img src="/source/winners/14.png" alt="Roll Roys"></div>            </div>
        </div>
        <div class="button-holder"><a href="#join_us" class="button fancybox">Apply now</a></div>
    </div>
</section>