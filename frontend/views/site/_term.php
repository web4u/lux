<div class="quote-slide">
    <div class="quote-photo"><img src="<?=$img?>" alt="<?=$name?>"></div>
    <div class="quote-text"><?=$text?></div>
    <div class="quote-author"><span class="gold"><?=$name?>   |   <?=$position?> </span>
        <?=$intro?></div>
</div>