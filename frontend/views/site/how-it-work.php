<?php
$seo=\backend\models\Seo::find()->where(['element'=>'pages', 'element_id'=>4])->one();
$this->title = $seo->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $seo->description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $seo->keywords
]);

$page=\backend\models\Pages::findOne(4);
$word=\backend\models\Words::find()->where(['page_id'=>4])->all();

$page_image=json_decode($page['image']);
$pics=json_decode($page['bg']);
?>
<section class="main">
    <div class="bg-slider bg-slider-mini">
        <?php
        if (count($pics)<=0) {
            ?>
            <div class="item"><img src="/source/pages/banner1.jpg" alt=""></div>
            <?php
        }
        foreach ($pics as $pic){
            ?>
            <div class="item"><img src="<?= $pic ?>" alt=""></div>
            <?php
        }
        ?>
    </div>
    <div class="holder">
        <div class="title">
            <h1><?=$page['title']?></h1>
            <div class="subtitle"><?=$word[0]['text']?></div>
        </div>
        <div class="how">
            <?php
            $k=1;
            $slog=['Fill out the form (5 min)', 'Wait, it will take 5 days', '', '5 min', 'Up to 2 week', ''];
            $items=\backend\models\Blokitems::find()->where(['blok_id'=>4])->all();
            foreach ($items as $item) {
                $class=$k%2==0?'how-item-reverse':'';
            ?>
            <div class="how-item rotate <?=$class?>">
                <div class="how-top">
                    <div class="how-title"><?=$item['text']?></div>
                    <div class="how-text"><?=$item['other_text']?></div>
                </div>
                <div class="how-icon how-icon<?=$k?>">
                    <img src="<?=json_decode($item['image'])[0]?>" alt="">
                    <div class="how-num"><?=$k?></div>
                </div>
                <div class="how-content">
                    <div class="how-float">
                        <div class="how-text"><?=$slog[$k-1]?></div>
                    </div>
                </div>
            </div>
            <? $k++; } ?>
        </div>
        <div class="button-holder"><a href="#join_us" class="button fancybox">Apply now</a></div>
    </div>
</section>