<?php
$seo=\backend\models\Seo::find()->where(['element'=>'pages', 'element_id'=>6])->one();
$this->title = $seo->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $seo->description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $seo->keywords
]);

$page=\backend\models\Pages::findOne(6);
$page_image=json_decode($page['image']);
$pics=json_decode($page['bg']);
?>
<section class="main">
    <div class="bg-slider bg-slider-mini">
        <?php
        if (count($pics)<=0) {
            ?>
            <div class="item"><img src="/source/pages/banner1.jpg" alt=""></div>
            <?php
        }
        foreach ($pics as $pic){
            ?>
            <div class="item"><img src="<?= $pic ?>" alt=""></div>
            <?php
        }
        ?>
    </div>
        <div class="title">
            <h1><?=$page['title']?></h1>
        </div>
        <div class="news-holder">
            <div id="result">
                <?=$news?>
            </div>
            <?php
            if ($count>9){
            ?>
            <div class="all-news">
                <a href="javascript:void(0);" data-table="news" data-full="<?=$count?>" data-toggle="load-more" data-plus="9" data-next="9" class="all-news-link">load more</a>
            </div>
            <? } ?>
        </div>
        <div class="button-holder" style="margin-top: 50px;"><a href="#join_us" class="button fancybox">Apply now</a></div>
</section>