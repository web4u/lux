<?php
$seo=\backend\models\Seo::find()->where(['element'=>'news', 'element_id'=>$id])->one();
$this->title = $seo->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $seo->description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $seo->keywords
]);

$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $news['title']
]);

$this->registerMetaTag([
    'property' => 'og:description',
    'content' => \frontend\models\Helper::getShortText(strip_tags($news['text']), 100)
]);

$this->registerMetaTag([
    'property' => 'og:url',
    'content' => '//'.Yii::$app->request->hostName.Yii::$app->request->url
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => '//'.Yii::$app->request->hostName.json_decode($news['image'])[0]
]);


$page=\backend\models\Pages::findOne(6);
$page_image=json_decode($page['image']);
$pics=json_decode($page['bg']);
?>
<section class="main">
    <div class="bg-slider bg-slider-mini">
        <?php
        if (count($pics)<=0) {
            ?>
            <div class="item"><img src="/source/pages/banner1.jpg" alt=""></div>
            <?php
        }
        foreach ($pics as $pic){
            ?>
            <div class="item"><img src="<?= $pic ?>" alt=""></div>
            <?php
        }
        ?>
    </div>
    <div class="holder">
        <article class="article">
            <h1 class="article-title"><?=$news['title']?></h1>
            <?php
            $pics=json_decode($news['image']);
            if (count($pics)>1){
            ?>
            <div class="article-slider-block">
                <div class="article-slider">
                    <?php
                    foreach ($pics as $pic){
                    ?>
                    <div class="article-slide">
                        <img src="<?=$pic?>" alt="<?=$news['title']?>">
                    </div>
                    <? } ?>
                </div>
                <ul class="slick-thumbs">
                    <?php
                    foreach ($pics as $pic){
                    ?>
                    <li class="article-slide">
                        <img src="<?=$pic?>" alt="<?=$news['title']?>">
                    </li>
                    <? } ?>
                </ul>
            </div>
            <? } ?>
            <img src="<?=$pics[0]?>" class="img-to-text" alt="<?=$news['title']?>">
            <?=$news['text']?>
        </article>
        <div class="article-bottom">
            <?php
            $date=new \DateTime($news['data']);
            ?>
            <div class="article-date"><?=\frontend\models\Helper::getMonth($date->format('m')*1) ?> <?=$date->format('d') ?>, <?=$date->format('Y') ?></div>
            <div class="article-share">
                Share now
                <!-- uSocial -->
                <script async src="https://usocial.pro/usocial/usocial.js?v=6.1.4" data-script="usocial" charset="utf-8"></script>
                <div class="uSocial-Share" data-pid="0a002bfde7db0d324a0c86141d65b087" data-type="share" data-options="round,style3,default,absolute,horizontal,size32,counter0" data-social="fb,twi,gPlus,pinterest" data-mobile="vi,wa,telegram,sms"></div>
                <!-- /uSocial -->
            </div>
        </div>

        <div class="title">
            <h2>Latest news</h2>
        </div>
        <div class="news-holder no-margin">
            <div class="news-line">
                <?php
                foreach ($next_news as $next_new) {
                ?>
                <a href="<?=\yii\helpers\Url::to('/news/'.\frontend\models\Helper::str2url($next_new['title']).'-'.$next_new['id']) ?>" class="news-item news-item-min">
                    <div class="image"><img src="<?=json_decode($next_new['image'])[0]?>" alt="<?=$next_new['title']?>"></div>
                    <span class="news-info"><?=$next_new['title']?></span>
                </a>
                <? } ?>
            </div>
        </div>
    </div>
</section>