<?php
$page=\backend\models\Pages::findOne(5);
$page_image=json_decode($page['image']);
$pics=json_decode($page['bg']);
?>
<section class="main">
    <div class="bg-slider bg-slider-mini">
        <?php
        if (count($pics)<=0) {
            ?>
            <div class="item"><img src="/source/pages/banner1.jpg" alt=""></div>
            <?php
        }
        foreach ($pics as $pic){
            ?>
            <div class="item"><img src="<?= $pic ?>" alt=""></div>
            <?php
        }
        ?>
    </div>
    <div class="holder" id="go_back">
        <div class="title">
            <h1><?=$category_name?> WINNERS</h1>
        </div>
        <div class="winner-category">
            <div class="winner-category-item">
                <div class="winner-category-holder">
                    <a href="/winners/all" class="winner-category-link"><?=isset($_SESSION['year'])?$_SESSION['year']:'All years';?></a>
                    <ul class="winner-category-hidden">
                        <li><?= \yii\helpers\Html::a('All years',
                                ['/site/delete-filter'], [
                                    'data-method' => 'POST',
                                    'data-params' => [
                                        'filter'=>'year'
                                    ],
                                ]) ?></li>
                        <?php
                        foreach ($years as $year){
                        ?>
                        <li><?= \yii\helpers\Html::a($year['year'],
                                ['/site/save-filter'], [
                                    'data-method' => 'POST',
                                    'data-params' => [
                                        'value' => $year['year'],
                                        'filter'=>'year'
                                    ],
                                ]) ?></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="winner-category-item">
                <div class="winner-category-holder">
                    <a href="" class="winner-category-link"><?=\frontend\models\Helper::getShortText($category_drop_name, 14)?></a>
                    <ul class="winner-category-hidden">
                        <li><a href="/winners/all">All categories</a></li>
                        <?php
                        foreach ($category as $cat){
                            ?>
                            <li><?= \yii\helpers\Html::a($cat['name'],
                                    ['/site/save-filter'], [
                                        'data-method' => 'POST',
                                        'data-params' => [
                                            'value' => $cat['id'],
                                            'filter'=>'category'
                                        ],
                                    ]) ?></li>
                            <?
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="winner-category-item">
                <div class="winner-category-holder">
                    <?
                    if (isset($_SESSION['country'])){
                        $countr=\backend\models\Country::findOne($_SESSION['country'])['name'];
                    } else {
                        $countr='All countries';
                    }
                    ?>
                    <a href="" class="winner-category-link"><?=$countr?></a>
                    <ul class="winner-category-hidden">
                        <li><?= \yii\helpers\Html::a('All countries',
                                ['/site/delete-filter'], [
                                    'data-method' => 'POST',
                                    'data-params' => [
                                        'filter'=>'country'
                                    ],
                                ]) ?></li>
                        <?php
                        foreach ($country as $c){
                        ?>
                        <li><?= \yii\helpers\Html::a($c['name'],
                                ['/site/save-filter'], [
                                    'data-method' => 'POST',
                                    'data-params' => [
                                        'value' => $c['id'],
                                        'filter'=>'country'
                                    ],
                                ]) ?></li>
                        <? } ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="winner-brand">
            <div class="winner-brand-list">
                <?=$winners?>
            </div>
            <?php
            if ($count>16){
            ?>
            <div class="load-more">
                <a href="">load more</a>
            </div>
            <? } ?>
        </div>
        <div class="button-holder"><a href="#join_us" class="button fancybox">Apply now</a></div>
    </div>
</section>
<?php
unset($_SESSION['year']);
unset($_SESSION['country']);
unset($_SESSION['category']);
?>