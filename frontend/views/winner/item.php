<?php
$seo=\backend\models\Seo::find()->where(['element'=>'winners', 'element_id'=>$win['id']])->one();
$this->title = $seo->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $seo->description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $seo->keywords
]);

$page=\backend\models\Pages::findOne(5);
$pics=json_decode($page['bg']);
?>
<section class="main">
    <div class="bg-slider bg-slider-mini">
        <?php
        if (count($pics)<=0) {
            ?>
            <div class="item"><img src="/source/pages/banner1.jpg" alt=""></div>
            <?php
        }
        foreach ($pics as $pic){
            ?>
            <div class="item"><img src="<?= $pic ?>" alt=""></div>
            <?php
        }
        ?>
    </div>
    <div class="holder">
        <div class="title">
            <h1><?=$win['name']?></h1>
        </div>
        <article class="article">
            <div class="article-logo">
                <img src="<?=json_decode($win['logo'])[0]?>" alt="<?=$win['name']?>">
            </div>
            <?=$win['text']?>
        </article>
        <div class="text-center">
            <?php
            $pics=json_decode($win['image']);
            foreach ($pics as $pic){
            ?>
            <img src="<?=$pic?>" alt="<?=$win['name']?>">
            <? } ?>
        </div>
        <div class="article-video">
            <div class="article-video-block">
                <?=$win['video']?>
            </div>
            <div class="article-share" style="text-align: right">
                Share now
                <!-- uSocial -->
                <script async src="https://usocial.pro/usocial/usocial.js?v=6.1.4" data-script="usocial" charset="utf-8"></script>
                <div class="uSocial-Share" data-pid="0a002bfde7db0d324a0c86141d65b087" data-type="share" data-options="round,style3,default,absolute,horizontal,size32,counter0" data-social="fb,twi,gPlus,pinterest" data-mobile="vi,wa,telegram,sms"></div>
                <!-- /uSocial -->
            </div>
        </div>
        <?php
        if (count($news)>0){
        ?>
        <div class="title">
            <h2><?=$win['name']?> news</h2>
        </div>
        <div class="news-holder">
            <div class="news-line">
                <?php
                foreach ($news as $item) {
                ?>
                <a href="/news/<?=\frontend\models\Helper::str2url($item['title']).'-'.$item['id']?>" class="news-item news-item-min">
                    <div class="image"><img src="<?=json_decode($item['image'])[0]?>" alt="<?=$item['title']?>"></div>
                    <span class="news-info"><?=$item['title']?></span>
                </a>
                <? } ?>
            </div>
            <?php
            if ($count>6){
            ?>
            <div class="load-more">
                <a href="">load more</a>
            </div>
            <? } ?>
        </div>
        <? } ?>
        <div class="button-holder"><a href="#join_us" class="button fancybox">Apply now</a></div>
    </div>
</section>