<a href="<?=$url?>" class="winner-brand-item">
    <span class="winner-brand-img">
        <img src="<?=$img?>" alt="<?=$name?>">
    </span>
    <span class="winner-brand-text"><?=$name?></span>
</a>