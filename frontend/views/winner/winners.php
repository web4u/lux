<?php
$seo=\backend\models\Seo::find()->where(['element'=>'pages', 'element_id'=>5])->one();
$this->title = $seo->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $seo->description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $seo->keywords
]);
$page=\backend\models\Pages::findOne(5);
$page_image=json_decode($page['image']);
$pics=json_decode($page['bg']);
?>
<section class="main">
    <div class="bg-slider bg-slider-mini">
        <?php
        if (count($pics)<=0) {
            ?>
            <div class="item"><img src="/source/pages/banner1.jpg" alt=""></div>
            <?php
        }
        foreach ($pics as $pic){
            ?>
            <div class="item"><img src="<?= $pic ?>" alt=""></div>
            <?php
        }
        ?>
    </div>
    <div class="holder">
        <div class="title">
            <h1>Winners</h1>
        </div>
        <div class="article-list">
            <?=$category?>
        </div>
        <div class="winner-year">
            <a href="/winners/all" class="winner-year-item active">All years</a>
            <?php
            foreach ($years as $year){
            ?>
            <a href="/winners/years/<?=$year['year']?>" class="winner-year-item"><?=$year['year']?></a>
            <? } ?>
        </div>
        <div class="winner-country">
            <div class="winner-country-item active">
                <a href="/winners/all">
                    <span class="winner-country-flag"><img src="images/media/flag/1.png" alt=""></span>
                    <span class="winner-country-name">All countries</span>
                </a>
            </div>
            <?php
            foreach ($country as $count){
                $pic=json_decode($count['icon']);
            ?>
            <div class="winner-country-item">
                <a href="<?=\yii\helpers\Url::to('/winners/country/'.$count['id'])?>">
                    <span class="winner-country-flag"><img src="<?=$pic[0]?>" alt="<?=$count['name']?>"></span>
                    <span class="winner-country-name"><?=$count['name']?></span>
                </a>
            </div>
            <? } ?>
        </div>
        <div class="winner-letter">
            <?php
            $winners=\backend\models\Winners::find()->all();
            $bukvar=[];
            foreach ($winners as $win){
                if (!in_array(strtolower(substr($win['name'], 0, 1)), $bukvar)){
                    array_push($bukvar, strtolower(substr($win['name'], 0, 1)));
                }
            }
            sort($bukvar);
            foreach ($bukvar as $litera){
                if (mb_detect_encoding($litera)!='UTF-8'){
            ?>
            <a href="#tab_<?=$litera?>" class="winner-letter-item"><?=strtoupper($litera)?></a>
            <? } } ?>
        </div>
        <?php
        $l=0;
        foreach ($bukvar as $litera){
            $wins=\backend\models\Winners::find()->where(['like', 'name', $litera.'%', false])->all();
        ?>
        <div class="winner-result <? if ($l==0) echo 'show'; ?>" id="tab_<?=$litera?>">
            <?php
            foreach ($wins as $win){
                $category=\backend\models\Category::findOne($win['category']);
            ?>
            <div class="winner-result-item"><a href="/winners/<?=\frontend\models\Helper::str2url($category['name']).'-'.$category['id']?>/<?=\frontend\models\Helper::str2url($win['name']).'-'.$win['id']?>"><?=$win['name']?></a></div>
            <? } ?>
        </div>
        <? $l++; }?>


        <div class="button-holder"><a href="#join_us" class="button fancybox">Apply now</a></div>
    </div>
</section>