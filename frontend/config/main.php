<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'homeUrl'=>'/',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl'=>''
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'symbol'=>'site/symbol',
                'compare-packages'=>'site/compare-packages',
                'benefits'=>'site/benefits',
                'benefits/<id:\d+>'=>'site/benefit',
                'how-it-work'=>'site/how-it-work',
                'winners'=>'winner/winners',
                'winners/all'=>'winner/all',
                'winners/<pref:[a-zA-Z0-9_-]+>'=>'winner/category',
                'winners/country/<id:\d+>'=>'winner/country',
                'winners/years/<year:\d+>'=>'winner/years',
                'winners/<category:[a-zA-Z0-9_-]+>/<pref:[a-zA-Z0-9_-]+>'=>'winner/item',
                'news/<pref:[a-zA-Z0-9_-]+>'=>'news/item',
            ],
        ],
    ],
    'params' => $params,
];
