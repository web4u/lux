<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'js/jquery.mCustomScrollbar.min.css',
        'css/all.css',
        'css/jquery.fancybox.css',
        'css/slick.css',
        'vendors/validate/css/validationEngine.jquery.css',
    ];
    public $js = [
        'js/slick.min.js',
        'js/jquery.fancybox.js',
        'js/jquery.validate.js',
        'js/jquery.uniform.min.js',
        'js/jquery.countTo.js',
        'js/jquery.mCustomScrollbar.js',
        'vendors/validate/js/jquery.validationEngine.js',
        'vendors/validate/js/languages/jquery.validationEngine-en.js',
        'vendors/jquery.formValid.js',
        'js/all.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
