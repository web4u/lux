(function ($) {
    $.fn.formValid = function (options) {
        options = $.extend({
            error_class: 'error'
        }, options);
        var self=$(this);
        self.submit(function (e) {
            var er = 0;
            $(this).find('*[data-valid]').each(function () {
                var pattern = ($(this).attr('data-valid')).split(':');
                switch (pattern[0]){
                    case 'email':
                        var exm = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                        if (!exm.test($(this).val())){
                            $(this).addClass(options.error_class);
                            er++;
                        }
                        break;

                    case 'string':
                        if (($.trim($(this).val())).length==0){
                            $(this).addClass(options.error_class);
                            er++;
                        }
                        if (pattern[1]!=undefined){
                            if (($.trim($(this).val())).length<pattern[1]){
                                $(this).addClass(options.error_class);
                                er++;
                            }
                        }

                        if (pattern[2]!=undefined){
                            if (($.trim($(this).val())).length>pattern[2]){
                                $(this).addClass(options.error_class);
                                er++;
                            }
                        }
                        break;

                    case 'number':
                        var exm = /^([0-9])+$/i;
                        var tp=$(this).val();
                        if (!exm.test($(this).val())){
                            $(this).addClass(options.error_class);
                            er++;
                        }
                        if (pattern[1]!=undefined){
                            if (tp.length<pattern[1]){
                                $(this).addClass(options.error_class);
                                er++;
                            }
                        }

                        if (pattern[2]!=undefined){
                            if (tp.length>pattern[2]){
                                $(this).addClass(options.error_class);
                                er++;
                            }
                        }
                        break;
                }
            });
            if (er > 0) {
                e.preventDefault();
                setTimeout(function () {
                    $(document).find('*[data-valid]').removeClass(options.error_class);
                }, 2000);
            }
        });
        return this;
    };
})(jQuery);
