$(document).ready(function(){
	if (window.location.hash=='#go_back'){
		console.log($("#go_back").offset().top);
		$("body,html").animate({
			scrollTop: $("#go_back").offset().top-80
		}, 500);
	} else if (window.location.search=='?thanks=null'){
		$(".modal-thank, .backdoor").fadeIn();
        setTimeout(function () {
            $(".modal-thank, .backdoor").fadeOut();
        }, 10000);
	}

	// fancybox
	jQuery('.fancybox').fancybox();
	
	// Radio
	$(".radio, .check").uniform();
	
	// Fixed bars
	$(window).scroll( function(){
		if ($(this).scrollTop() > 100){
			$('.fixed-top').addClass('fixed');	
		}else{
			$('.fixed-top').removeClass('fixed');
		}
		
		if ($(window).scrollTop() > 300){
			$('.fixed-bot').removeClass('fixed');
		}else{
			$('.fixed-bot').addClass('fixed');
		}
		
		if($(window).scrollTop() + $(window).height() <= $(document).height() - 200) {
			//$('.fixed-bot').removeClass('fixed');
		}else{
			$('.fixed-bot').addClass('fixed');
		}
		
		if($(window).scrollTop() > $(document).height() - $(window).height()*1.75) {
			$('.fixed-scroll-top').addClass('fixed');
		}else{
			$('.fixed-scroll-top').removeClass('fixed');
		}
		
	});
	
	// mobile nav
	$(document).on('click','.geo-mob',function(e){
		e.preventDefault();
		$('.mob-nav').removeClass('active');
		$('.mob-nav2').addClass('active');
	});
	$(document).on('click','.nav-mob',function(e){
		e.preventDefault();
		$('.mob-nav').removeClass('active');
		$('.mob-nav1').addClass('active');
	});
	$(document).on('click','.close-mob',function(e){
		e.preventDefault();
		$('.mob-nav').removeClass('active');
	});
	// slider
	$('.article-slider').slick({
		dots: false,
		arrows: true,
		autoplay: false,
		speed: 1000,
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.slick-thumbs'
	});
	$('.slick-thumbs').slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		asNavFor: '.article-slider',
		dots: false,
		centerMode: false,
		focusOnSelect: true
	});
	$('.human-slider').slick({
		dots: true,
		arrows: false,
		autoplay: true,
        autoplaySpeed: 2000,
		slidesToShow: 4,
		slidesToScroll: 4,
		responsive: [{
			breakpoint: 960,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			}
		},{
			breakpoint: 767,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		},{
			breakpoint: 560,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});
	$('.quote-slider').slick({
		dots: false,
		arrows: true,
		autoplay: true,
        autoplaySpeed: 2000,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<button type="button" class="slick-prev">Prev</button>'
	});

	$('.bg-slider').slick({
		dots: false,
		arrows: true,
		autoplay: true,
        autoplaySpeed: 4000,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<div class="slide-bg-left"></div>',
		nextArrow: '<div class="slide-bg-right"></div>',
	});
		
	
	
	// catalog height	
	function news_height(){
		var n_height = $('.news-item-min').width();
		$('.news-item-big').css('height', n_height + 'px');
	}
	news_height();
	$(window).resize(function(){
		news_height();
	});
	// scroll to top	
	$(document).on('click','.fixed-scroll-top',function(e){
		e.preventDefault();
        $('body, html').animate({
			scrollTop: 0
		}, 1000);
	});
	
	// sorting hidden
	$(document).on('click','.sorting-link',function(e){
		e.preventDefault();
		if($(this).hasClass('active')){
			$('.sorting-block-hidden').hide();
			$('.sorting-link').removeClass('active');
		}else{
			$('.sorting-block-hidden').hide();
			$('.sorting-link').removeClass('active');
			$(this).next('.sorting-block-hidden').show();
			$(this).addClass('active');
		}
	});
	// winner-category hidden
	$(document).on('click','.winner-category-link',function(e){
		e.preventDefault();
		if($(this).hasClass('active')){
			$('.winner-category-hidden').hide();
			$('.winner-category-link').removeClass('active');
		}else{
			$('.winner-category-hidden').hide();
			$('.winner-category-link').removeClass('active');
			$(this).next('.winner-category-hidden').show();
			$(this).addClass('active');
		}
	});
	$(document).bind( "mouseup touchend", function(e){
		var container = $('.winner-category-item');
		if (!container.is(e.target) // if the target of the click isn't the container...
			&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			$('.winner-category-hidden').hide();
			$('.winner-category-link').removeClass('active');
		}
	});
	
	// icons animation
	$('.how-item').on('mouseenter',function(e){
		$(this).addClass('rotate');
	});
	
	// count animation
	$(window).load( function(e){
		$('.count').countTo();
	});

	if ($("*").is(".count-to")) {
        $(window).scroll(function (e) {
            var document_top = $(document).scrollTop();
            var number_offset = $('.count-to').offset().top - $(window).height() + ($(window).height()) / 4;
            if (document_top > number_offset) {
                $('.count').countTo();
                $('.count-to').removeClass('count');
            }
            ;
        });
    }
	
	/*** tabs leter ***/
	$(".winner-letter a").click(function (e) {
		e.preventDefault();
		$(".winner-result").removeClass('show');
		$($(this).attr('href')).addClass('show');
    });

	/*** Custom scrollbar drop-down ***/
	$(".winner-category-hidden").mCustomScrollbar();

	/*** Load More ***/
	$("a[data-toggle=load-more]").click(function () {
		var table=$(this).attr('data-table');
		var plus=$(this).attr('data-next');
		var full=Number($(this).attr('data-full'));
		var dod=Number($(this).attr('data-plus'));
		$(this).attr('data-next',Number(plus)+dod);
		$.ajax({
			url: '/site/ajax',
			type: 'post',
			data: {table: table, plus: plus, '_csrf-frontend': $("meta[name=csrf-token]").attr('content')},
			success: function (res) {
				$("#result").append(res);
				if (Number(plus)+dod>=full){
                    $("a[data-toggle=load-more]").hide();
				}
            }
		})
    });

	if (sessionStorage.sub==undefined&&sessionStorage.sub!='1'&&$(window).width()>768){
		setTimeout(function () {
			$(".subscribe-modal, .backdoor").fadeIn();
        },30000);
        sessionStorage.sub=1;
	}

	$(".backdoor, .subscribe-modal .close").click(function () {
        $(".subscribe-modal, .backdoor, .help-modal").fadeOut();
    })

	$("#subscribe").submit(function (e) {
		e.preventDefault();
		if ($(this).find('.formError').size()<=0) {
            $.ajax({
                url: '/site/subscribe',
                type: 'post',
                data: {name: $(this).find('input[name=name]').val(), email: $(this).find('input[name=email]').val(), '_csrf-frontend': $("meta[name=csrf-token]").attr('content')},
                success: function (res) {
                    $(".subscribe-modal").fadeOut();
                    $(".modal-thank").fadeIn();
                    setTimeout(function () {
                        $(".modal-thank, .backdoor").fadeOut();
                    }, 10000);
                }
            })
        }
    });

    $("form#subscribe").validationEngine();

    $("#help").formValid();

    $("#show-help").click(function () {
		$(".backdoor, .help-modal").fadeIn();
    })
});	
