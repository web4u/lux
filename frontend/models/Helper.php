<?php

namespace frontend\models;

use backend\models\Kurs;
use backend\models\Lang;
use backend\models\News;
use backend\models\Blokitems;
use Yii;
use yii\base\Model;
use TelegramBot;
use yii\helpers\Url;

/**
 * ContactForm is the model behind the contact form.
 */
class Helper extends Model
{
    public function getShortText($text, $lenght){
        $text=strip_tags($text);
        if(mb_strlen($text, 'UTF-8')>$lenght)
        {
            $text = mb_substr($text, 0, $lenght, 'UTF-8');
            return $text.'...';
        }
        else
            return $text;
    }

    public function getMonth($month){
        $arr=[
            1 => "Jan",
            2 => "Feb",
            3 => "March",
            4 => "Apr",
            5 => "May",
            6 => "Jul",
            7 => "Jun",
            8 => "Aug",
            9 => "Sept",
            10 => "Oct",
            11 => "Nov",
            12 => "Dec",
        ];

        return $arr[$month];
    }

    public function getPicture($pic, $all=true){
        $img=json_decode($pic);
        if (empty($img[0])){
            return false;
        }
        return $all==true?$img:$img[0];
    }

    public function rus2translit($string) {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        return strtr($string, $converter);
    }
    public function str2url($str) {
        // переводим в транслит
        $str = self::rus2translit($str);
        // в нижний регистр
        $str = strtolower($str);
        // заменям все ненужное нам на "-"
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        // удаляем начальные и конечные '-'
        $str = trim($str, "-");
        return $str;
    }

    /*** TODO Пагинация ***/
    public function paging($active, $table='News', $count=6){
        $class='\backend\models\\'.$table;
        $all=$class::find()->count();
        $page=ceil($all/$count);
        $prev=$active==1?1:$active-1;
        $next=$active==$page?$page:$active+1;
        $html='<ul class="navigation-pages">';
        $last=$page>10?10:$page;
        $first=1;
        if ($active>5){
            $first=$active-5;
            if ($active+5>$page){
                $last=$page;
            } else {
                $last=$active+4;
            }
        }
        for ($i=$first;$i<=$last;$i++){
            if ($i==$active) {
                $html .= '<li class="active"><a href="'.Url::to('news?page='.$i).'">'.$i.'</a></li>';
            } else {
                $html.='<li><a href="'.Url::to('news?page='.$i).'">'.$i.'</a></li>';
            }
        }

        $html.='</ul>';

        return $html;
    }

    public function translate($str, $lang){
        if ($lang=='ukr'){
            return $str;
        } else {
            $res = Lang::find()->where(['ukr' => $str])->one();
            if (!empty($res)) {
                return $res['rus'];
            } else {
                return $str;
            }
        }
    }

    public function lang_path($flag='lang'){
        $url=$_SERVER['REQUEST_URI'];
        $lang=strpos($url, '/ru')!==false?'rus':'ukr';
        $path=$lang=='ukr'?'/':'/ru/';
        return $flag=='lang'?$lang:$path;
    }

    public function usd2uah($suma){
        $kurs=Kurs::find()->where(['currency_from'=>'USD', 'currency_to'=>'UAH', 'data'=>date("Y-m-d")])->one();
        return ceil($suma*$kurs['value']);
    }

    public function bot($id, $text){
        $bot = new TelegramBot\Api\BotApi('424331503:AAH-OGeKIyQIi_Y_W0EgVdDBorIktTkIy-s');
        $response = $bot->sendMessage($id, $text);
    }
}
