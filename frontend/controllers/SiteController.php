<?php
namespace frontend\controllers;

use backend\models\Category;
use backend\models\Employees;
use backend\models\Media;
use backend\models\News;
use backend\models\Orders;
use backend\models\Packageitems;
use backend\models\Packages;
use backend\models\Settings;
use backend\models\Subscribe;
use backend\models\Testimonials;
use backend\models\Winners;
use frontend\models\Helper;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $winners_html='';
        $winners=Winners::find()->limit(15)->all();
        foreach ($winners as $winner){
            $winners_html.=$this->renderPartial('_winners', ['img'=>json_decode($winner['logo'])[0], 'name'=>$winner['name']]);
        }

        $emp_html='<div class="human-slide">';
        $emp=Employees::find()->all();
        $k=1;
        foreach ($emp as $item){
            $emp_html.='<div class="human-item">
                        <div class="human-photo"><img src="'.json_decode($item['photo'])[0].'" alt="'.$item['name'].'"></div>
                        <div class="human-text"><span>'.$item['name'].'</span>
                            '.$item['position'].'</div>
                    </div>';
            if ($k%3==0){
                $emp_html.='</div><div class="human-slide">';
            }
            $k++;
        }
        $emp_html.='</div>';

        /*** отзывы ***/
        $term_html='';
        $terms=Testimonials::find()->limit(10)->all();
        foreach ($terms as $term){
            $term_html.=$this->renderPartial('_term', ['img'=>json_decode($term['photo'])[0], 'name'=>$term['name'], 'position'=>$term['position'], 'intro'=>$term['intro'], 'text'=>$term['text']]);
        }

        /*** медиа ***/
        $media_html='';
        $media=Media::find()->all();
        foreach ($media as $term){
            $media_html.=$this->renderPartial('_med', ['img'=>json_decode($term['logo'])[0], 'name'=>$term['name']]);
        }

        return $this->render('index', [
            'winners'=>$winners_html,
            'emp'=>$emp_html,
            'term'=>$term_html,
            'media'=>$media_html
        ]);
    }

    public function actionSymbol(){
        return $this->render('symbol');
    }

    public function actionBenefits(){
        return $this->render('benefits', [
            'packages'=>Packages::find()->all()
        ]);
    }

    public function actionBenefit($id){
        return $this->render('benefit', [
            'id'=>$id,
            'pack'=>Packages::findOne($id),
            'pack_items'=>Packageitems::find()->where(['package_id'=>$id])->all(),
            'packages'=>Packages::find()->where('id!=:id', ['id'=>$id])->all()
        ]);
    }

    public function actionHowItWork(){
        return $this->render('how-it-work');
    }

    public function actionSaveFilter(){
        $sess=Yii::$app->session;
        $sess[$_POST['filter']]=$_POST['value'];
        return $this->redirect(substr_count(Yii::$app->request->referrer, 'go_back')>0?Yii::$app->request->referrer:Yii::$app->request->referrer.'#go_back');
    }

    public function actionDeleteFilter(){
        Yii::$app->session->remove($_POST['filter']);
        return $this->redirect(substr_count(Yii::$app->request->referrer, 'go_back')>0?Yii::$app->request->referrer:Yii::$app->request->referrer.'#go_back');
    }

    public function actionOrderSave(){
        $model=new Orders();
        $model->name=$_POST['Orders']['name'];
        $model->company=$_POST['Orders']['company'];
        $model->email=$_POST['Orders']['email'];
        $model->phone=$_POST['Orders']['phone'];
        $model->web=$_POST['Orders']['web'];
        $model->comment=$_POST['Orders']['comment'];
        $model->save();
        $to=Settings::findOne(1)['value'];
        $subject = "Новый заказ";
        $message = ' 
        <html>  
            <head> 
                <title>Новый заказ</title> 
            </head> 
            <body> 
                <p>На сайте был оставлен новый заказ</p>  
                <p><b>Name:</b> '.$_POST['Orders']['name'].'</p>
                <p><b>Company:</b> '.$_POST['Orders']['company'].'</p>
                <p><b>Email:</b> '.$_POST['Orders']['email'].'</p>
                <p><b>Phone:</b> '.$_POST['Orders']['phone'].'</p>
                <p><b>Website:</b> '.$_POST['Orders']['web'].'</p>
                <p><b>Comment:</b> '.$_POST['Orders']['comment'].'</p>
            </body> 
        </html>';

        $headers  = "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "From: ".$_SERVER['SERVER_NAME']." <support@".$_SERVER['SERVER_NAME'].">\r\n";
        mail($to, $subject, $message, $headers);

        if (isset($_POST['Orders']['subscribe'])){
            $sub=new Subscribe();
            $sub->email=$_POST['Orders']['email'];
            $sub->save();
        }
        return $this->redirect(['/'.Yii::$app->request->referrer, 'thanks' => 'null']);
    }

    public function actionSubscribe(){
        $sub=new Subscribe();
        $sub->email=isset($_POST['Subscribe']['email'])?$_POST['Subscribe']['email']:$_POST['email'];
        $sub->name=isset($_POST['name'])?$_POST['name']:'anonim';
        $sub->save();
        if (isset($_POST['Subscribe']['email'])) {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionAjax(){
        $table=$_POST['table'];
        $offset=$_POST['plus'];
        switch ($table){
            case 'winners':
                $where=[];
                if (isset($_SESSION['year'])){
                    $where['year']=$_SESSION['year'];
                }
                if (isset($_SESSION['country'])){
                    $where['country']=$_SESSION['country'];
                }
                if (isset($_SESSION['category'])){
                    $where['category']=$_SESSION['category'];
                } else {
                }
                $win_html='';
                $wins=Winners::find()->where($where)->orderBy('id DESC')->offset($offset)->limit(16)->all();
                foreach ($wins as $win) {
                    $cat=Category::find()->where(['id'=>$win['category']])->one();
                    $win_html.=$this->renderPartial('_win', [
                        'url'=>Url::to('/winners/'.Helper::str2url($cat['name']).'-'.$cat['id'].'/'.Helper::str2url($win['name']).'-'.$win['id']),
                        'img'=>Helper::getPicture($win['logo'], false),
                        'name'=>$win['name']
                    ]);
                }
                return $win_html;
                break;

            case 'news':
                $html='<div class="news-line">';
                $news=News::find()->where(['status'=>1])->orderBy('data')->offset($offset)->limit(9)->all();
                $k=1;
                foreach ($news as $row){
                    $class='news-item-big';
                    $pic=Helper::getPicture($row['image']);
                    $html.='<a href="'.Url::to('news/'.Helper::str2url($row['title'])).'-'.$row['id'].'" class="news-item '.$class.'">
                    <div class="image"><img src="'.$pic[0].'" alt="'.$row['title'].'"></div>
                    <span class="news-info">'.$row['title'].'</span>
                </a>';
                    if ($k==9){
                        $html.='</div>';
                    }
                    $k++;
                }
                return $html;
                break;
        }
    }

    public function actionHelp(){
        $to='admin@web4u.in.ua';
        $subject = "Новый запрос на помощь";
        $message = ' 
        <html>  
            <head> 
                <title>Новый запрос на помощь</title> 
            </head> 
            <body> 
                <p>На сайте был оставлен новый запрос на помощь</p>  
                <p><b>Name:</b> '.$_POST['name'].'</p>
                <p><b>Phone:</b> '.$_POST['phone'].'</p>
            </body> 
        </html>';

        $headers  = "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "From: ".$_SERVER['SERVER_NAME']." <support@".$_SERVER['SERVER_NAME'].">\r\n";
        mail($to, $subject, $message, $headers);

        return $this->render(Yii::$app->request->referrer);
    }
    
    public function actionComparePackages(){
        return $this->render('compare');
    }
}
