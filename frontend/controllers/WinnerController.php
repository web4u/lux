<?php
namespace frontend\controllers;

use backend\models\Category;
use backend\models\Country;
use backend\models\News;
use backend\models\WinnerCategory;
use backend\models\Winners;
use frontend\models\Helper;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class WinnerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionWinners()
    {
        $category_html='';
        $category=Category::find()->orderBy('srt DESC')->all();
        foreach ($category as $cat){
            $category_html.=$this->renderPartial('_category', [
                'url'=>Url::to('winners/'.Helper::str2url($cat['name']).'-'.$cat['id']),
                'img'=>Helper::getPicture($cat['photo'], false),
                'name'=>$cat['name']
            ]);
        }
        return $this->render('winners', [
            'category'=>$category_html,
            'years'=>Winners::find()->where('year!=""')->distinct('year')->orderBy('year DESC')->groupBy('year')->all(),
            'country'=>Country::find()->orderBy('srt DESC')->all(),
        ]);
    }

    public function actionCategory($pref){
        $id=explode('-', $pref);
        $id=array_pop($id);
        $win_html='';
        $where['category']=$id;
        if (isset($_SESSION['year'])){
            $where['year']=$_SESSION['year'];
        }
        if (isset($_SESSION['country'])){
            $where['country']=$_SESSION['country'];
        }
        if (isset($_SESSION['category'])){
            $where['category']=$_SESSION['category'];
            $cat_name=Category::findOne($_SESSION['category'])['name'];
        } else {
            $cat_name=Category::findOne($id)['name'];
        }
        $wins=Winners::find()->where($where)->orderBy('id DESC')->limit(16)->all();
        foreach ($wins as $win) {
            $win_html.=$this->renderPartial('_win', [
                'url'=>Url::to('/winners/'.$pref.'/'.Helper::str2url($win['name']).'-'.$win['id']),
                'img'=>Helper::getPicture($win['logo'], false),
                'name'=>$win['name']
            ]);
        }
        return $this->render('category', [
            'winners'=>$win_html,
            'years'=>Winners::find()->where('year!=""')->distinct('year')->groupBy('year')->orderBy('year DESC')->all(),
            'country'=>Country::find()->orderBy('name')->all(),
            'pref'=>$pref,
            'category'=>Category::find()->orderBy('name')->all(),
            'category_name'=>Category::findOne($id)['name'],
            'category_drop_name'=>$cat_name,
            'count'=>Winners::find()->where($where)->count()
        ]);
    }

    public function actionCountry($id){
        $win_html='';
        $where['country']=$id;
        if (isset($_SESSION['year'])){
            $where['year']=$_SESSION['year'];
        }
        if (isset($_SESSION['category'])){
            $where['category']=$_SESSION['category'];
            $cat_name=Category::findOne($_SESSION['category'])['name'];
        } else {
            $cat_name='All categories';
        }
        $sess=Yii::$app->session;
        $sess['country']=$id;
        $wins=Winners::find()->where($where)->orderBy('id DESC')->limit(16)->all();
        foreach ($wins as $win) {
            $cat=Category::findOne($win['category']);
            $win_html.=$this->renderPartial('_win', [
                'url'=>Url::to('/winners/'.Helper::str2url($cat['name']).'-'.$cat['id'].'/'.Helper::str2url($win['name']).'-'.$win['id']),
                'img'=>Helper::getPicture($win['logo'], false),
                'name'=>$win['name']
            ]);
        }
        return $this->render('category', [
            'winners'=>$win_html,
            'years'=>Winners::find()->where('year!=""')->distinct('year')->groupBy('year')->orderBy('year DESC')->all(),
            'country'=>Country::find()->orderBy('name')->all(),
            'category'=>Category::find()->orderBy('name')->all(),
            'category_name'=>Country::findOne($id)['name'],
            'category_drop_name'=>$cat_name,
            'count'=>Winners::find()->where($where)->count()
        ]);
    }

    public function actionYears($year){
        $win_html='';
        $where['year']=$year;
        if (isset($_SESSION['country'])){
            $where['country']=$_SESSION['country'];
        }
        if (isset($_SESSION['category'])){
            $where['category']=$_SESSION['category'];
        }
        $sess=Yii::$app->session;
        $sess['year']=$year;
        $wins=Winners::find()->where($where)->orderBy('id DESC')->limit(16)->all();
        foreach ($wins as $win) {
            $cat=Category::findOne($win['category']);
            $win_html.=$this->renderPartial('_win', [
                'url'=>Url::to('/winners/'.Helper::str2url($cat['name']).'-'.$cat['id'].'/'.Helper::str2url($win['name']).'-'.$win['id']),
                'img'=>Helper::getPicture($win['logo'], false),
                'name'=>$win['name']
            ]);
        }
        return $this->render('category', [
            'winners'=>$win_html,
            'years'=>Winners::find()->where('year!=""')->distinct('year')->groupBy('year')->orderBy('year DESC')->all(),
            'country'=>Country::find()->orderBy('name')->all(),
            'category'=>Category::find()->orderBy('name')->all(),
            'category_name'=>$year,
            'category_drop_name'=>'Category',
            'count'=>Winners::find()->where($where)->count()
        ]);
    }

    public function actionItem($category, $pref){
        $id=explode('-', $pref);
        $id=array_pop($id);
        $item=Winners::findOne($id);
        return $this->render('item', [
            'win'=>$item,
            'news'=>News::find()->where(['winner'=>$id])->limit(6)->all(),
            'count'=>News::find()->where(['winner'=>$id])->count(),
        ]);
    }

    public function actionAll(){
        $where=[];
        if (isset($_SESSION['year'])){
            $where['year']=$_SESSION['year'];
        }
        if (isset($_SESSION['country'])){
            $where['country']=$_SESSION['country'];
        }
        if (isset($_SESSION['category'])){
            $where['category']=$_SESSION['category'];
            $cat_name=Category::findOne($_SESSION['category'])['name'];
        } else {
            $cat_name='All categories';
        }
        $win_html='';
        $wins=Winners::find()->where($where)->orderBy('id DESC')->limit(16)->all();
        foreach ($wins as $win) {
            $cat=Category::find()->where(['id'=>$win['category']])->one();
            $win_html.=$this->renderPartial('_win', [
                'url'=>Url::to('/winners/'.Helper::str2url($cat['name']).'-'.$cat['id'].'/'.Helper::str2url($win['name']).'-'.$win['id']),
                'img'=>Helper::getPicture($win['logo'], false),
                'name'=>$win['name']
            ]);
        }
        return $this->render('all', [
            'winners'=>$win_html,
            'years'=>Winners::find()->where('year!=""')->distinct('year')->groupBy('year')->orderBy('year DESC')->all(),
            'country'=>Country::find()->orderBy('name')->all(),
            'category'=>Category::find()->orderBy('name')->all(),
            'count'=>Winners::find()->where($where)->count(),
            'category_name'=>$cat_name
        ]);
    }
}
