<?php
namespace frontend\controllers;

use backend\models\News;
use frontend\models\Helper;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $html='<div class="news-line">';
        $news=News::find()->where(['status'=>1])->orderBy('data')->limit(9)->all();
        $k=1;
        foreach ($news as $row){
            $class='news-item-big';
            $pic=Helper::getPicture($row['image']);
            $html.='<a href="'.Url::to('news/'.Helper::str2url($row['title'])).'-'.$row['id'].'" class="news-item '.$class.'">
                    <div class="image"><img src="'.$pic[0].'" alt="'.$row['title'].'"></div>
                    <span class="news-info">'.$row['title'].'</span>
                </a>';
            if ($k==9){
                $html.='</div>';
            }
            $k++;
        }
        return $this->render('index', [
            'news'=>$html,
            'count'=>News::find()->where(['status'=>1])->count()
        ]);
    }

    public function actionItem($pref){
        $id=explode('-', $pref);
        $id=array_pop($id);
        $news=News::findOne($id);
        return $this->render('item', ['news'=>$news, 'next_news'=>News::find()->where('id!=:id',['id'=>$id])->limit(3)->all(), 'id'=>$id]);
    }
}
