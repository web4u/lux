<?php
namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class Winners extends Widget
{
    private $html;

    public function init()
    {
        parent::init();
        $winners=\backend\models\Winners::find()->limit(15)->all();
        foreach ($winners as $winner){
            $this->html.='<div class="brands-item"><img src="'.json_decode($winner['logo'])[0].'" alt="'.$winner['name'].'"></div>';
        }
    }

    public function run()
    {
        return $this->html;
    }
}