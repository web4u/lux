<?php
namespace frontend\widgets;

use backend\models\Bloks;
use yii\base\Widget;

class Text extends Widget
{
    public $id;
    public $flag;
    private $html;

    public function init()
    {
        parent::init();
        $res=Bloks::findOne($this->id);
        if ($this->flag=='image'){
            $this->html=json_decode($res[$this->flag])[0];
        } else {
            $this->html = $res[$this->flag];
        }
    }

    public function run()
    {
        return $this->html;
    }
}