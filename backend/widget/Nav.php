<?php
namespace backend\widget;

use yii\base\Widget;
use yii\db\Query;
use yii\helpers\Html;

class Nav extends Widget
{
    private $html;
    public $table;
    public $active;

    public function init()
    {
        parent::init();

        $prev=$this->active==1?'':'onclick="location.href=\'?page='.($this->active-1).'\'"';

        $this->html='<div class="btn-group"><button '.$prev.' type="button" class="btn btn-white"><i class="fa fa-chevron-left"></i></button>';

        $res=(new Query())
            ->select("*")
            ->from($this->table)
            ->count();

        $page=ceil($res/20);

        $start=$this->active<5?1:$this->active-4;

        for ($i=$start;$i<=$page&&$i<=$start+9;$i++){
            $class=$this->active==$i?'active':'';
            $this->html.='<a href="?page='.$i.'" class="btn btn-white '.$class.'">'.$i.'</a>';
        }

        $next=$this->active<$page?'onclick="location.href=\'?page='.($this->active+1).'\'"':'';

        $this->html.='<button type="button" '.$next.' class="btn btn-white"><i class="fa fa-chevron-right"></i></button></div>';
    }

    public function run()
    {
        return $this->html;
    }
}
?>