<?php
namespace backend\widget;

use yii\base\Widget;
use yii\helpers\Html;

class Menu extends Widget
{
    private $menu;
    public $active;

    public function init()
    {
        parent::init();

        $tmp=explode('?', $this->active);
        $tmp=explode('/', $tmp[0]);
        $this->active=!isset($tmp[2])?'':'/'.$tmp[2];

        $arr=[
            [
                'name'=>'Главная',
                'pref'=>'',
                'icon'=>'fa-home'
            ],
            [
                'name'=>'Победители',
                'pref'=>'winners',
                'icon'=>'fa-trophy'
            ],
            [
                'name'=>'Категории',
                'pref'=>'category',
                'icon'=>'fa-bars'
            ],
            [
                'name'=>'Судьи',
                'pref'=>'employees',
                'icon'=>'fa-users'
            ],
            [
                'name'=>'Отзывы',
                'pref'=>'testimonials',
                'icon'=>'fa-comments'
            ],
            [
                'name'=>'Медиа',
                'pref'=>'media',
                'icon'=>'fa-newspaper-o'
            ],
            [
                'name'=>'Новости',
                'pref'=>'news',
                'icon'=>'fa-newspaper-o'
            ],
            [
                'name'=>'Страны',
                'pref'=>'country',
                'icon'=>'fa-flag'
            ],
            [
                'name'=>'Доп. тексты',
                'pref'=>'bloks',
                'icon'=>'fa-file-o',
                'sub'=>[
                    'bloks'=>'Заголовки',
                    'blokitems'=>'Доп. тексты'
                ]
            ],
            [
                'name'=>'Пакеты',
                'pref'=>'packages',
                'icon'=>'fa-star',
                'sub'=>[
                    'packages'=>'Пакеты',
                    'packageitems'=>'Описание пакетов'
                ]
            ],
            [
                'name'=>'Страницы',
                'pref'=>'pages',
                'icon'=>'fa-file-o'
            ],
            [
                'name'=>'Заказы',
                'pref'=>'orders',
                'icon'=>'fa-star'
            ],
            [
                'name'=>'Общие выражения',
                'pref'=>'full_words',
                'icon'=>'fa-font'
            ],
            [
                'name'=>'Настройки',
                'pref'=>'settings',
                'icon'=>'fa-cogs'
            ]
        ];

        foreach ($arr as $row){
            $sub=[];
            if (!empty($row['sub'])){
	            foreach ($row['sub'] as $k=>$v){
	                array_push($sub, $k);
	            }
            }
            $class=$this->active==$row['pref']||in_array($this->active, $sub)?'active':'';
            if (!empty($row['sub'])){
                $this->menu.='<li class="'.$class.'"><a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">'.$row['name'].'</span><span class="fa arrow"></span></a><ul class="nav nav-second-level collapse">';
                foreach ($row['sub'] as $key=>$value) {
                    $this->menu .= '<li><a href="'.\Yii::$app->homeUrl.$key.'">'.$value.'</a></li>';
                }

                $this->menu.='</ul></li>';
            } else {
                $this->menu .= '<li class="' . $class . '"><a href="' . \Yii::$app->homeUrl . $row['pref'] . '"><i class="fa ' . $row['icon'] . '"></i> <span class="nav-label">' . $row['name'] . '</span></a></li>';

            }
        }
    }

    public function run()
    {
        return $this->menu;
    }
}
?>