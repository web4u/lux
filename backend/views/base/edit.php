<?php
$this->title=$title;
use dosamigos\tinymce\TinyMce;
use dosamigos\datetimepicker\DateTimePicker;
use dosamigos\datepicker\DatePicker;
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->title ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= Yii::$app->homeUrl ?>"><?= \backend\models\Help::Lang()->index ?></a>
            </li>
            <li>
                <a href="<?= Yii::$app->homeUrl ?><?= $table ?>"><?= $module ?></a>
            </li>
            <li class="active">
                <strong><?= $this->title ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php
                    $form=\yii\bootstrap\ActiveForm::begin();
                    ?>
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-0" aria-expanded="true"> Основная информация</a></li>
                            <?php
                            if (isset($model::$tabs)){
                                $t=1;
                                foreach ($model::$tabs as $key=>$value) {
                                    ?>
                                    <li><a data-toggle="tab" href="#tab-more-<?=$t?>" aria-expanded="true"> <?=$key?></a></li>
                                    <?php
                                    $t++;
                                }
                            }
                            ?>
                        </ul>
                        <div class="tab-content" style="padding-top: 20px;">
                            <div id="tab-0" class="tab-pane active">
                                <div class="panel-body">
                                    <?php
                                    foreach ($rows as $row){
                                        switch ($row['type']){
                                            case 'input':
                                                $param=empty($row['attr'])?[]:$row['attr'];
                                                echo $form->field($model, $row['name'])->textInput($param);
                                                break;

                                            case 'select':
                                                if (!empty($row['table'])) {
                                                    if (isset($row['attr']['multiple'])) {
                                                        $model->$row['name']=json_decode($model->$row['name']);
                                                        echo $form->field($model, $row['name'])->dropDownList(\backend\models\Help::getOption($row['table']), ['prompt' => \backend\models\Help::Lang()->select, 'class' => 'select2 form-control', 'multiple'=>'true']);
                                                    } else {
                                                        echo $form->field($model, $row['name'])->dropDownList(\backend\models\Help::getOption($row['table']), ['prompt' => \backend\models\Help::Lang()->select, 'class' => 'select2 form-control']);
                                                    }
                                                } else {
                                                    if (isset($row['attr']['multiple'])) {
                                                        $model->$row['name']=json_decode($model->$row['name']);
                                                        echo $form->field($model, $row['name'])->dropDownList($row['data'], ['prompt' => \backend\models\Help::Lang()->select, 'class' => 'select2 form-control', 'multiple'=>'true']);
                                                    } else {
                                                        echo $form->field($model, $row['name'])->dropDownList($row['data'], ['prompt' => \backend\models\Help::Lang()->select, 'class' => 'select2 form-control']);
                                                    }
                                                }
                                                break;

                                            case 'url':
                                                if (!empty($row['table'])) {
                                                    echo $form->field($model, $row['name'])->dropDownList(\backend\models\Help::getOption($row['table']), ['prompt' => 'оберіть']);
                                                } else {
                                                    echo $form->field($model, $row['name'])->dropDownList($row['data'], ['prompt' => 'оберіть']);
                                                }
                                                break;

                                            case 'editor':
                                                echo $form->field($model, $row['name'])->widget(TinyMce::className(), [
                                                    'options' => ['rows' => 20],
                                                    'clientOptions' => [
                                                        'plugins' => [
                                                            "advlist autolink lists link charmap print preview anchor",
                                                            "searchreplace visualblocks code fullscreen",
                                                            "insertdatetime media table contextmenu paste image"
                                                        ],
                                                        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media",
                                                        'external_filemanager_path' => '/admin/plugins/responsivefilemanager/',
                                                        'filemanager_title' => 'Файловый менеджер',
                                                        'external_plugins' => [
                                                            //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                                                            'filemanager' => '/admin/plugins/responsivefilemanager/plugin.min.js',
                                                            //Иконка/кнопка загрузки файла в панеле иснструментов.
                                                            'responsivefilemanager' => '/admin/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                                                        ],
                                                    ]
                                                ]);
                                                break;

                                            case 'datepicker':
                                                echo $form->field($model, $row['name'])->widget(
                                                    'trntv\yii\datetime\DateTimeWidget',
                                                    [
                                                        'phpDatetimeFormat' => 'yyyy-MM-dd',
                                                        'clientOptions' => [
                                                            'minDate' => new \yii\web\JsExpression('new Date("2015-01-01")'),
                                                            'allowInputToggle' => false,
                                                            'sideBySide' => true,
                                                            'locale' => 'ru-ru',
                                                            'widgetPositioning' => [
                                                                'horizontal' => 'auto',
                                                                'vertical' => 'auto'
                                                            ]
                                                        ]
                                                    ]);
                                                break;

                                            case 'datetimepicker':
                                                echo $form->field($model, $row['name'])->widget(
                                                    'trntv\yii\datetime\DateTimeWidget',
                                                    [
                                                        'momentDatetimeFormat' => 'DD-MM-YYYY HH:mm',
                                                        'clientOptions' => [
                                                            'minDate' => new \yii\web\JsExpression('new Date("'.date("Y-m-d H:i").'")'),
                                                            'allowInputToggle' => false,
                                                            'sideBySide' => true,
                                                            'locale' => 'ru-ru',
                                                            'widgetPositioning' => [
                                                                'horizontal' => 'auto',
                                                                'vertical' => 'auto'
                                                            ]
                                                        ]
                                                    ]);
                                                break;

                                            case 'checkbox':
                                                echo $form->field($model, $row['name'])->checkbox();
                                                break;

                                            case 'textarea':
                                                echo $form->field($model, $row['name'])->textarea(['rows'=>5]);
                                                break;

                                            case 'file':
                                                ?>
                                                <input type="hidden" value="" name="<?= ucfirst($table) ?>[<?= $row['name'] ?>][]">
                                                <div class="form-group">
                                                    <label><?= $label[$row['name']] ?></label>
                                                    <div>
                                                        <input type="hidden" id="input_file_<?= $row['name'] ?>" data-name="<?= ucfirst($table) ?>[<?= $row['name'] ?>][]">
<!--                                                        <button type="button" data-toggle="modal" data-target="#filemanager" class="btn btn-primary">Прикріпити файли</button>-->
                                                        <a href="/admin/plugins/responsivefilemanager/dialog.php?type=2&field_id=input_file_<?= $row['name'] ?>&lang=ru_RU" class="btn iframe-btn btn-primary" type="button">Прикрепить файл</a>
                                                    </div>
                                                    <div class="loadedImage clearfix">
                                                        <?php

                                                        $files=json_decode($model[$row['name']]);
                                                        if (!empty($files)) {
                                                            foreach ($files as $file) {
                                                                ?>
                                                                <div class="item">
                                                                    <i class="fa fa-trash" onclick="$(this).parent().remove();"></i>
                                                                    <img src="<?= $file ?>" class="img-thumbnail">
                                                                    <input type="hidden" value="<?= $file ?>"
                                                                           name="<?= ucfirst($table) ?>[<?= $row['name'] ?>][]">
                                                                </div>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>

                                                <?php
                                                break;

                                            case 'legend':
                                                ?>
                                                <div class="form-group">
                                                    <div class="alert alert-info">
                                                        <?php
                                                        foreach ($row['list'] as $key=>$value){
                                                            ?>
                                                            <div><?= $value ?></div>
                                                            <?
                                                        }
                                                        ?>
                                                    </div>
                                                </div>

                                                <?php
                                                break;

                                            case 'tags':
                                                echo $form->field($model, $row['name'])->textInput(['data-role'=>'tagsinput', 'id'=>'input_'.$row['name']]);
                                                ?>
                                                <script>
                                                    $(function(){
                                                        var citynames = new Bloodhound({
                                                            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                                                            queryTokenizer: Bloodhound.tokenizers.whitespace,
                                                            prefetch: {
                                                                url: '/admin/site/json-tags?table='.$row['table'].'&row='.$row['row'],
                                                                filter: function(list) {
                                                                    return $.map(list, function(cityname) {
                                                                        return { name: cityname }; });
                                                                }
                                                            }
                                                        });
                                                        citynames.initialize();

                                                        $('#input_<?=$row['name']?>').tagsinput({
                                                            typeaheadjs: {
                                                                name: 'citynames',
                                                                displayKey: 'name',
                                                                valueKey: 'name',
                                                                source: citynames.ttAdapter()
                                                            }
                                                        });
                                                    });
                                                </script>
                                                <?
                                                break;
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                            if (isset($model::$tabs)){
                                $t=1;
                                foreach ($model::$tabs as $key=>$value) {
                                    ?>
                                    <div id="tab-more-<?=$t?>" class="tab-pane">
                                        <div class="panel-body">
                                            <? require 'tabs/'.$value.'.php'; ?>
                                        </div>
                                    </div>
                                    <?php
                                    $t++;
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <button class="btn btn-success" style="margin-top: 20px;"><?= \backend\models\Help::Lang()->save ?></button>
                    <?php
                    \yii\bootstrap\ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>