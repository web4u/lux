<?php
$seo=\backend\models\Seo::find()->where(['element_id'=>$id, 'element'=>$table])->one();
$title=!empty($seo)?$seo['title']:'';
$desc=!empty($seo)?$seo['description']:'';
$key=!empty($seo)?$seo['keywords']:'';
?>
<div class="form-group">
    <label>Title</label>
    <input type="text" class="form-control" name="Seo[title]" value="<?=$title?>">
</div>

<div class="form-group">
    <label>Description</label>
    <textarea class="form-control" rows="10" name="Seo[description]"><?=$desc?></textarea>
</div>

<div class="form-group">
    <label>Keywords</label>
    <textarea class="form-control" rows="10" name="Seo[keywords]"><?=$key?></textarea>
</div>