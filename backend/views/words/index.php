<?php
$this->title='Выражения';
use dosamigos\tinymce\TinyMce;
use dosamigos\datetimepicker\DateTimePicker;
use dosamigos\datepicker\DatePicker;
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->title ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= Yii::$app->homeUrl ?>">Главная</a>
            </li>
            <li class="active">
                <strong>Общие выражения</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h1>Форма</h1>
                    <?php
                    $form=\yii\bootstrap\ActiveForm::begin(['action'=>'', 'options'=>['class'=>'form-horizontal']]);

                    $res=\backend\models\Words::find()->where(['and', ['page_id'=>0], ['in', 'id', [29,30,31,32]]])->all();
                    foreach ($res as $row){
                        ?>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type="text" class="form-control" name="Words[<?= $row['id'] ?>]" value="<?= $row['text'] ?>">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                    <?php } ?>
                    <h1>Футер сайта</h1>
                    <?php

                    $res=\backend\models\Words::find()->where(['and', ['page_id'=>0], ['in', 'id', [33,34,35,36,37,38,39,40,41,42,43,44,45]]])->all();
                    foreach ($res as $row){
                        ?>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type="text" class="form-control" name="Words[<?= $row['id'] ?>]" value="<?= $row['text'] ?>">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                    <?php } ?>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">Сохранить</button>
                        </div>
                    </div>
                    <? \yii\bootstrap\ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>