<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Pages extends  ActiveRecord{

    static $tabs=['Seo'=>'seo', 'Выражения'=>'words'];

    public function nameTable(){
        return 'Страницы сайта';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'title'=>'Название',
            'bg'=>'Фото',
            'image'=>'Дополнительные изображения',
        ];
    }

    public function rules()
    {
        return [
            [['title'],'required'],
            [['bg'], 'safe']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'title',
                'type'=>'input',
                'display'=>true,
            ],
            [
                'name'=>'bg',
                'type'=>'file',
                'display'=>false,
            ],
            [
                'name'=>'image',
                'type'=>'file',
                'display'=>false,
            ]
        ];
    }

}