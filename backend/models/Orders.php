<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Orders extends  ActiveRecord{

    public function nameTable(){
        return 'Заказы';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'name'=>'Имя',
            'phone'=>'Телефон',
            'email'=>'Email',
            'data'=>'Дата заказа',
            'web'=>'Сайт',
            'company'=>'Компания',
            'comment'=>'Комментарий',
        ];
    }

    public function rules()
    {
        return [
            [['email', 'name', 'company', 'web','phone'],'required'],
            ['email', 'email'],
            [['data', 'comment'], 'safe']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'name',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'company',
                'type'=>'input',
                'display'=>true,
            ],
            [
                'name'=>'email',
                'type'=>'input',
                'display'=>true,
            ],
            [
                'name'=>'phone',
                'type'=>'input',
                'display'=>true,
            ],
            [
                'name'=>'web',
                'type'=>'input',
                'display'=>false,
            ],
            [
                'name'=>'comment',
                'type'=>'textarea',
                'display'=>false,
            ],
            [
                'name'=>'data',
                'type'=>'datetimepicker',
                'display'=>true,
            ]
        ];
    }

}