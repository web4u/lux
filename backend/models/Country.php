<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Country extends  ActiveRecord{

    public function nameTable(){
        return 'Страны';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'name'=>'Название',
            'icon'=>'Флаг',
            'srt'=>'Сортировка',
        ];
    }

    public function rules()
    {
        return [
            [['name'],'required'],
            [['icon', 'srt'], 'safe']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'name',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'icon',
                'type'=>'file',
                'display'=>true
            ],
            [
                'name'=>'srt',
                'type'=>'input',
                'display'=>false
            ]
        ];
    }

}