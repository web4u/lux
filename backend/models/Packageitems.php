<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Packageitems extends  ActiveRecord{

    public function nameTable(){
        return 'Описание пакетов';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'text'=>'Текст',
            'image'=>'Фото',
            'package_id'=>'Пакет',
        ];
    }

    public function rules()
    {
        return [
            [['text', 'image'],'required'],
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'package_id',
                'type'=>'select',
                'display'=>true,
                'table'=>[
                    'name'=>'packages',
                    'value'=>'id',
                    'text'=>'name'
                ]
            ],
            [
                'name'=>'text',
                'type'=>'editor',
                'display'=>true
            ],
            [
                'name'=>'image',
                'type'=>'file',
                'display'=>true
            ]
        ];
    }

}