<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Employees extends  ActiveRecord{

    public function nameTable(){
        return 'Сотрудники';
    }

    public function actionRow(){
        return [
            'copy'=>'Копировать',
        ];
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'name'=>'Имя',
            'position'=>'Должность',
            'photo'=>'Фото',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'position'],'required'],
            [['photo'],'safe']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'name',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'position',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'photo',
                'type'=>'file',
                'display'=>false
            ]
        ];
    }

}