<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Blokitems extends  ActiveRecord{

    public function nameTable(){
        return 'Позиции блоков';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'blok_id'=>'Блок',
            'image'=>'Картинка',
            'text'=>'Текст',
            'other_text'=>'Дополнительный текст',
        ];
    }

    public function rules()
    {
        return [
            [['blok_id', 'text'],'required'],
            [['image', 'other_text'], 'safe']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'blok_id',
                'type'=>'select',
                'display'=>true,
                'table'=>[
                    'name'=>'bloks',
                    'value'=>'id',
                    'text'=>'name'
                ]
            ],
            [
                'name'=>'image',
                'type'=>'file',
                'display'=>true
            ],
            [
                'name'=>'text',
                'type'=>'editor',
                'display'=>true
            ],
            [
                'name'=>'other_text',
                'type'=>'editor',
                'display'=>false
            ]
        ];
    }

}