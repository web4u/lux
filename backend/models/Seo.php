<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Seo extends  ActiveRecord{

    public function nameTable(){
        return 'SEO';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'element'=>'Элемент',
            'element_id'=>'ID элемента',
            'title'=>'Title',
            'keywords'=>'Keywords',
            'description'=>'Description',
        ];
    }

    public function rules()
    {
        return [
            [['element', 'element_id', 'title', 'keywords', 'description'], 'safe']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'element',
                'type'=>'input',
                'display'=>true,
            ],
            [
                'name'=>'element_id',
                'type'=>'input',
                'display'=>true,
            ],
            [
                'name'=>'title',
                'type'=>'input',
                'display'=>true,
            ],
            [
                'name'=>'keywords',
                'type'=>'textarea',
                'display'=>false,
            ],
            [
                'name'=>'description',
                'type'=>'textarea',
                'display'=>false,
            ]
        ];
    }
}