<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Packages extends  ActiveRecord{

    static $tabs=['SEO'=>'seo'];

    public function nameTable(){
        return 'Пакеты';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'name'=>'Название',
            'intro'=>'Краткое описание',
            'desc'=>'Полное описание',
            'price'=>'Цена',
            'recomended'=>'Рекомендуемый',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'intro', 'desc', 'price'],'required'],
            [['recomended'], 'safe']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'name',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'intro',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'desc',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'price',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'recomended',
                'type'=>'checkbox',
                'display'=>true
            ]
        ];
    }

}