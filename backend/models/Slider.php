<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Slider extends  ActiveRecord{

    public function nameTable(){
        return 'Слайды';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'image'=>'Картинка',
            'srt'=>'Порядок вывода',
        ];
    }

    public function rules()
    {
        return [
            [['image', 'srt'], 'safe']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'image',
                'type'=>'file',
                'display'=>true
            ],
            [
                'name'=>'srt',
                'type'=>'input',
                'display'=>true
            ]
        ];
    }

}