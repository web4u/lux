<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use frontend\models\Helper;
use yii\db\ActiveRecord;


class Testimonials extends  ActiveRecord{

    public function nameTable(){
        return 'Отзывы';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'name'=>'Имя',
            'position'=>'Должность',
            'intro'=>'Дополнительное описание',
            'text'=>'Текст',
            'photo'=>'Фото',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'position', 'intro', 'text'],'required'],
            [['photo'], 'safe']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'name',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'position',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'intro',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'text',
                'type'=>'editor',
                'display'=>false
            ],
            [
                'name'=>'photo',
                'type'=>'file',
                'display'=>false
            ]
        ];
    }

}