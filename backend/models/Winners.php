<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Winners extends  ActiveRecord{

    static $years=['2013'=>2013,'2014'=>2014,'2015'=>2015,'2016'=>2016,'2017'=>2017,'2018'=>2018,'2019'=>2019,'2020'=>2020,'2021'=>2021,'2022'=>2022,'2023'=>2023,'2024'=>2024,'2025'=>2025,'2026'=>2026,'2027'=>2027,'2028'=>2028,'2029'=>2029,'2030'=>2030,'2031'=>2031,'2032'=>2032,'2033'=>2033,'2034'=>2034,'2035'=>2035,'2036'=>2036,'2037'=>2037,'2038'=>2038,'2039'=>2039,'2040'=>2040];

    static $tabs=['SEO'=>'seo'];

    public function nameTable(){
        return 'Победители';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'name'=>'Название',
            'logo'=>'Логотип',
            'year'=>'Год',
            'country'=>'Страна',
            'category'=>'Категория',
            'image'=>'Фото',
            'video'=>'Видео',
            'text'=>'Текст',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'year', 'country', 'category', 'text'],'required'],
            [['logo', 'image', 'video'], 'safe']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'name',
                'type'=>'input',
                'display'=>true,
            ],
            [
                'name'=>'logo',
                'type'=>'file',
                'display'=>false,
            ],
            [
                'name'=>'year',
                'type'=>'select',
                'display'=>true,
                'data'=>self::$years
            ],
            [
                'name'=>'country',
                'type'=>'select',
                'display'=>true,
                'table'=>[
                    'name'=>'country',
                    'value'=>'id',
                    'text'=>'name'
                ]
            ],
            [
                'name'=>'category',
                'type'=>'select',
                'display'=>true,
                'table'=>[
                    'name'=>'category',
                    'value'=>'id',
                    'text'=>'name'
                ]
            ],
            [
                'name'=>'image',
                'type'=>'file',
                'display'=>false,
            ],
            [
                'name'=>'video',
                'type'=>'textarea',
                'display'=>false,
            ],
            [
                'name'=>'text',
                'type'=>'editor',
                'display'=>false,
            ]
        ];
    }

}