<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Category extends  ActiveRecord{

    public function nameTable(){
        return 'Категории';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'name'=>'Название',
            'photo'=>'Изображение',
            'srt'=>'Сортировка',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'photo'],'required'],
            [['srt'], 'safe']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'name',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'photo',
                'type'=>'file',
                'display'=>false
            ],
            [
                'name'=>'srt',
                'type'=>'input',
                'display'=>false
            ]
        ];
    }

}