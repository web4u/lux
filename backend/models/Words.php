<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Words extends  ActiveRecord{

    public function nameTable(){
        return 'Выражения';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'page_id'=>'Старница',
            'text'=>'Текст',
        ];
    }

    public function rules()
    {
        return [
            [['page_id', 'text'],'required'],
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'page_id',
                'type'=>'select',
                'display'=>true,
                'table'=>[
                    'name'=>'pages',
                    'value'=>'id',
                    'text'=>'title'
                ]
            ],
            [
                'name'=>'text',
                'type'=>'editor',
                'display'=>false
            ]
        ];
    }

}