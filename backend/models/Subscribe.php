<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Subscribe extends  ActiveRecord{

    public function nameTable(){
        return 'Подписки';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'email'=>'Email',

        ];
    }

    public function rules()
    {
        return [
            ['email','required'],
            ['email','email'],
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'email',
                'type'=>'input',
                'display'=>true
            ]
        ];
    }

}